#! /usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime as dt


def readFile(fname):
    """
    Argument
    --------
    fname (str): path to the file to read

    Return
    ------
    list of strings
    """
    with open(fname, "r") as f:
        content = f.readlines()
    return content


def readData(fname, headerlines=1):
    """
    Argument
    --------
    fname       (str): path to the file to read
    headerlines (int): number of headerlines to skip

    Return
    ------
    - tuple of datetime objects
    - tuple of float objects
    """

    data = []
    content = readFile(fname)
    dformat = "%Y-%m-%d %H:%M:%S"
    for i, line in enumerate(content):
        if i < headerlines:
            continue
        date, value = line.strip().split(",")
        try:
            date = dt.strptime(date, dformat)
            value = float(value)
            data.append([date, value])
        except ValueError:
            pass
    return zip(*data)

